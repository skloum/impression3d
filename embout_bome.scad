include <BOSL2/std.scad>
include <BOSL2/screws.scad>

$fn=100;

// dimensions mesurées
hauteur_totale_piece = 82;
r1_embout_exterieur = 22;
profondeur_embout_exterieur = 8;
rayon_rainure = 5;
taille_trou_vit_mulet = 14.5;

rayon_rond_attache = 9;
rayon_trou_attache = 3.1;
hauteur_totale_attache = 30; // depuis le centre du fond rond de la rainure
hauteur_attache_depuis_embout = 24;
profondeur_attache = 8;

rayon_embout_interieur = 14;
profondeur_embout_interieur = 20;
longueur_rectangle_interieur = 38;
largeur_rectangle_interieur = 13;
type_ecrou = "M4"; // Le diamètre du trou percé dans la bôme est de 5mm, il faut
                   // donc une vis avec un tête plus large
decalage_centre_trou_vis_ecrou = 12;

// valeurs devinées
r2_embout_exterieur = 18;
R_embout_exterieur = 50;

// dimensions calculées
largeur_pied_attache = profondeur_embout_exterieur;
distance_centre_rainure_par_rapport_base_embout = hauteur_totale_piece - hauteur_totale_attache;
longueur_embout_exterieur = hauteur_totale_piece - hauteur_attache_depuis_embout;
diagonale_vit_mulet = taille_trou_vit_mulet * sqrt(2);
longueur_trou_vis_ecrou = (longueur_rectangle_interieur - diagonale_vit_mulet) / 2;

//
// Définitions des éléments
//
module trou_vit_mulet() {
  rotate([0, 0, 45])
    square(taille_trou_vit_mulet, center=true);
}

module embout_exterieur() {
  linear_extrude(height=profondeur_embout_exterieur)
    difference() {
    // Création d'un «oeuf» centré sur le cercle de gauche
    egg(longueur_embout_exterieur, r1_embout_exterieur, r2_embout_exterieur, R_embout_exterieur, anchor="left");

    // Fond rond de la rainure
    translate([distance_centre_rainure_par_rapport_base_embout - r1_embout_exterieur, 0, 0])
      circle(rayon_rainure);
    // + partie droite au-dessus
    translate([distance_centre_rainure_par_rapport_base_embout - r1_embout_exterieur, -rayon_rainure, 0])
      square([hauteur_totale_attache, rayon_rainure * 2]);

    trou_vit_mulet();
  }
}

module trou_vis_ecrou() {
  epaisseur_ecrou = struct_val(nut_info(type_ecrou), "thickness");
  nut_trap_inline(epaisseur_ecrou + diagonale_vit_mulet / 2, type_ecrou)
    position(TOP)
    screw_hole(length=longueur_trou_vis_ecrou - epaisseur_ecrou, anchor=BOT, head="none");
}

module embout_interieur() {
  difference() {
    linear_extrude(height=profondeur_embout_interieur)
      difference() {
      union() {
        circle(rayon_embout_interieur);
        square([largeur_rectangle_interieur, longueur_rectangle_interieur], center=true);
      }

      trou_vit_mulet();
    }

    // Les 2 trous pour vis et écrou
    translate([0, 0, decalage_centre_trou_vis_ecrou])
      for (rotation_x = [90, -90]) {
        rotate([rotation_x, 30, 0])
          trou_vis_ecrou();
      }
  }
}

module attache() {
  linear_extrude(height=profondeur_attache)
    difference() {
    union() {
      // pied de l'attache
      square([largeur_pied_attache, hauteur_totale_attache - rayon_rond_attache]);
      // attache circulaire
      translate([rayon_rond_attache, hauteur_totale_attache - rayon_rond_attache])
        circle(rayon_rond_attache);

      // arc pour relier le rond de l'attache au pied
      difference() {
        // valeurs empiriques
        rayon_arc = 29;
        decalage_x_centre_rayon = 5;
        // elargissement rectangulaire du pied
        translate([largeur_pied_attache, 0])
          square([rayon_rond_attache * 2 - largeur_pied_attache, hauteur_attache_depuis_embout - rayon_rond_attache]);
        // cercle permettant d'obtenir l'arc de cercle entre le pied et le rond de l'attache
        translate([largeur_pied_attache + sqrt(pow(rayon_arc, 2) - pow(decalage_x_centre_rayon, 2)),
                   -decalage_x_centre_rayon])
          circle(rayon_arc);
      }
    }
    // trou de l'attache
    translate([rayon_rond_attache, hauteur_totale_attache - rayon_rond_attache])
      circle(rayon_trou_attache);
  }
}

//
// Assemblage des éléments
//
embout_exterieur();

translate([0, 0, profondeur_embout_exterieur])
embout_interieur();

for (position_y = [rayon_rainure, -(rayon_rainure + profondeur_attache)]) {
  translate([distance_centre_rainure_par_rapport_base_embout - r1_embout_exterieur, position_y, 0])
    rotate([0, -90, -90])
    attache();
}
